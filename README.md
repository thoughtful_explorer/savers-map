# Savers Map

This tool was designed to extract thrift store locations from the official [Savers](https://stores.savers.com/) and [Village des Valeurs](https://villagedesvaleurs.com) websites and place them into KML format. Savers, Value Village, and Village des Valeurs are all owned and operated by the same public corporation in North America, but the official website's store finding tool for some reason specifically excludes Village des Valeurs locations in Québec. Future scraping projects would likely include Savers locations in Australia. 

## Dependencies
* Python 3.x
    * Requests module for making https requests
    * Beautiful Soup 4.x (bs4) for scraping 
    * Simplekml for easily building KML files
    * Regular expressions module for pattern matching
    * Selenium for rendering JS to be able to access geodata associated with each location 
* Also of course depends on official [Savers](https://stores.savers.com) and [Village des Valeurs](https://villagedesvaleurs.com) websites.
