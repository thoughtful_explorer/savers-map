#!/usr/bin/python3

import requests
from bs4 import BeautifulSoup
from selenium import webdriver
import simplekml
import re

#Set filename/path for KML file output
kmlfile = "savers.kml"
#Set KML schema name
kmlschemaname = "savers"

#Returns reusable BeautifulSoup of a given site
def getsoupinit(url):
    #Start a chromium webdriver with the given page URL...
    #...in this case we need the js to be rendered to get the geodata associated with each location, so Selenium it is
    driver = webdriver.Chrome()
    driver.get(url)
    #Return soupified HTML
    return BeautifulSoup(driver.page_source, 'html.parser')

#Returns a list of all state/provincial sites
def getallsites():
    #The site for Québec is entirely separate...
    qcURL = "https://magasinage.villagedesvaleurs.com/qc/" 
    #...from the remainder of the English-speaking Value Village/Savers stores in North America, so we need two URLs
    remainderURL = "https://stores.savers.com"
    #Initialize a list to hold all the links
    links =[]
    #Initialize a soup of a list of all Québec site links that have the 'data-city-item' attribute - these are the links we're looking for
    qcsoup = getsoupinit(qcURL).find_all("a", attrs={'data-city-item':True})
    #Loop through each link in the Québec URL...
    for a in qcsoup:
        #...and append only the link URL from each of them to the links list
        links.append(a["href"])
    #...then do it again for the remainder (non-Québec site) URLs from the English-speaking page that have the 'ga-link' attribute - these are the links we're looking for
    remaindersoup = getsoupinit(remainderURL).find_all(attrs={"data-ga":re.compile("Maplist, Region")})
    #Note: If you happen to *only* want the Village des Valeurs locations in Québec, you could comment out the rest of this getallsites() function.
    #Loop through each link in the English-speaking Savers listed regions
    for region in remaindersoup:
        #Create another soup of this region's location list...
        soup = getsoupinit(region["href"]).find_all("a", attrs={'data-city-item':True})
        #...and append only the link URL from each of them to the links list
        for a in soup: 
            links.append(a["href"])
    return links

#Tests a given URL to see if the server responds. Returns True if so, raises an exception if not
def testurl(url):
	try:
		#Get Url
		get = requests.get(url)
		# if the request succeeds
		if get.status_code == 200:
			return True
		else:
			return False
	#Exception
	except requests.exceptions.RequestException as e:
		return False

#Returns a list of store names, addresses, and lat/lng for a given site url
def getsite(url):
    #Initialize the soup
    soup = getsoupinit(url)
    #Initialize stores list because we must be able to return more than one store per site in this function
    stores=[]
    #Get the relevant divs, identified by their div tag-unique styling
    divs = soup.find_all('div', class_="map-list-item-inner")
    for div in divs:
        #Get the name of the store by its link text, removing the first two characters (these are labeled with a single-digit numerical label followed by a period)
        name = div.a.get_text()[2:]
        #For the address, we need to winnow out all the other divs within this div...
        addressdiv = div.find('div', class_="address")
        #...and then add the actual address text parts of this together to make one string in which to present a typical address
        storeaddress = addressdiv.contents[1].get_text() + addressdiv.contents[3].get_text()
        #Get the coordinates by using the data-index attribute of the link within the div
        index = div.a['data-index']
        #Use the value of the data-index attribute to find its corresponding div in the leaflet map to get the lat/lng data
        mapdiv = soup.find('div', attrs={'data-index':index})
        lat = mapdiv['data-lat']
        lng = mapdiv['data-lng']
        #Append the 4 data points collected here into the array associated with this site
        stores.append([name,storeaddress,lat,lng])
    return stores

#Returns a list of store names, addresses, and lat/lng for all Village des Valeurs/Savers locations
def getallstores():
    #Initialize stores list
    allstores=[]
    #Loop through all the site URLs
    for site in getallsites():
        #First, test whether this URL is actually reachable...
        if testurl(site):
            #...and if so, append the store data returned from the regional site URL in the getsite() function...
            allstores.append(getsite(site))
        #...otherwise, print an error for the site that isn't responding.
        else:
            print("Site "+site+" is not responding")
    return allstores

#Saves a KML file of a given list of stores
def createkml(stores):
    #Initialize kml object
    kml = simplekml.Kml()
    #Iterate through list for each store
    for site in stores:
        for store in site: 
            #Get the name of the store
            storename = str(store[0])
            #Get coordinates of the store
            lat = store[2]
            lng = store[3]
            #First, create the point name and description in the kml
            point = kml.newpoint(name=storename,description=str(store[1]))
            #Finally, add coordinates to the feature
            point.coords=[(lng,lat)]
    #Save the final KML file
    kml.save(kmlfile)
#Bring it all together
createkml(getallstores())
